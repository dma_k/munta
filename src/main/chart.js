// Copyright 2017 Dmitry Katsubo, https://gitlab.com/dma_k/munta

//
// Factory/service that implements all operations with a chart.
// This service is stateful, so it is created using factory function.
//
muntaModule.factory('ChartManager', ['UtilityService', function(utilityService) {

	var currentYear,
		simulations, // number of vehicles/simulations added so far
		chartMode; // the same semantic as model.owner

	var simulationChart = {
		chart: {
			events: {
				load: function() {
					this.renderer.text('Flags description:<br><i>[B]</i> = business<br><i>[P]</i> = private<br><i>[L]</i> = leasing<br><i>[U]</i> = used', this.marginRight, this.chartHeight - 70).css({
						color:      'darkmagenta',
						fontWeight: 'bold'
					}).add();
					this.renderer.text('<i>[b]</i> = benzine<br><i>[p]</i> = PHEV<br><i>[d]</i> = diesel<br><i>[e]</i> = electro', this.marginRight + 90, this.chartHeight - 55).css({
						color:      'darkmagenta',
						fontWeight: 'bold'
					}).add();
				}
			}
		},
		title: {
			text: 'Vehicle expences'
		},
		legend: {
			x: 100,
			userHTML: true
		},
		xAxis: {
			title: {
				text: 'cummulative expences per year'
			}
		},
		yAxis: {
			title: {
				text: 'expences', // will be updated later depending on chart mode
			},
			labels: {
				formatter: function () {
					return utilityService.euroLabelFormatter(this.value);
				}
			},
			stackLabels: {
				enabled: true,
				style: {
					fontWeight: 'bold',
					color:      'maroon'
				},
				formatter: function() {
					// Don't show totals for negative stacks; solution from http://stackoverflow.com/a/34640614/267197
					if (!this.isNegative) {
						var negativeStack = this.axis.stacks["-column" + (this.stack ? this.stack : '')];
						return utilityService.euroLabelFormatter(negativeStack != null ? this.total + negativeStack[this.x].total : this.total);
					}
				}
			}
		},
		tooltip: {
			headerFormat: '<b>{point.x}:</b><br/>',
			pointFormat: '{series.name}: €{point.y}'
		},
		plotOptions: {
			spline: {
				dataLabels: {
					enabled:   true,
					color:     'crimson',
					formatter: function() {
						return utilityService.euroLabelFormatter(this.y);
					}
				}
			},
			column: {
				stacking:   'normal',
				dataLabels: {
					enabled:   true,
					color:     'indigo',
					formatter: function() {
						return utilityService.euroLabelFormatter(this.y);
					}
				}
			},
			series: {
				pointPadding: 0.05, // padding between each bar
				groupPadding: 0.05 // padding between bar groups
			}
		},
		colors: [ '#7CB5EC', '#434348', '#90ED7D', '#F7A35C', '#8085E9', '#F15C80', '#E4D354', '#FFA07A', '#2B908F', '#F45B5B', '#91E8E1', '#2F7ED8', '#0D233A', '#8BBC21', '#910000', '#1AADCE', '#F08080', '#492970', '#F28F43', '#77A1E5', '#C42525', '#A6C96A', '#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE', '#DB843D', '#92A8CD', '#A47D7C', '#B5CA92', '#A74B59', '#46C4B7', '#EDBA70' ],
		exporting: {
			fallbackToExportServer: false,
			sourceWidth:  1600,
			sourceHeight: 1200
		}
	};

	//
	// Reset all chart data.
	//
	var resetSimulationChart = function() {
		simulations = [];
		currentYear = new Date().getFullYear();
		chartMode = null;
		simulationChart.series = [];
		simulationChart.xAxis.categories = [];
	};

	//
	// Switch chart to business mode and reply all (private) simulations.
	//
	var switchToBusinessMode = function(config) {
		var models = simulations;
		resetSimulationChart();
		chartMode = 'business';
		models.forEach(function(model) {
			addSimulation(config, model);
		});
	};

	//
	// Calculate BPM (Vehicle registration) tax for given vehicle.
	//
	function getRegistrationTax(config, model) {
		var registrationInitialTax = 0;
		// If CO₂ emission is zero, then BPM is zero:
		if (model.coEmission > 0) {
			for (var i = 0; i < config.registrationTaxes.length; i++) {
				if (config.registrationTaxes[i].year == model.registrationYear && (!config.registrationTaxes[i].fuelType || config.registrationTaxes[i].fuelType == model.fuelType)) {
					var taxes = config.registrationTaxes[i].taxes;
					var taxIndex = 0;
					for (taxIndex = taxes.length - 1; taxIndex > 0; taxIndex--) {
						// Loop from the end of the array till element with index 1:
						if (model.coEmission >= taxes[taxIndex].coEmission) {
							break;
						}
					}
					// (T1)
					registrationInitialTax = taxes[taxIndex].basicTax + (model.coEmission - taxes[taxIndex].coEmission) * taxes[taxIndex].taxPerExtraGram;
					break;
				}
			}
			if (registrationInitialTax == 0) {
				console.warn('BPM tax for year ' + model.registrationYear + ', ' + model.coEmission + ' CO₂ was not found');
				return 0;
			}
		}

		var registrationTax = registrationInitialTax;

		// (T1) For new vehicles we assume that registrationTax = registrationInitialTax otherwise make a correction:
		if (model.presetProperty && model.presetProperty.secondHand) {
			registrationTax *= model.initialPrice / model.cataloguePrice;
			console.log('#' + simulations.length + ': BPM tax for registration year ' + model.registrationYear + ' is €' + registrationInitialTax + ' and for ' + currentYear + ' is €' + Math.round(registrationTax));
		} else {
			console.log('#' + simulations.length + ': BPM tax for registration year ' + model.registrationYear + ' is €' + registrationInitialTax);
		}

		return registrationTax;
	}

	//
	// Generate and add new chart data.
	//
	var addSimulation = function(config, model) {
		simulations.push(angular.extend({}, model));

		var seriesTitle;
		var secondHandFlag = false;
		model.year = currentYear;

		// Try to form the title from the current preset:
		if (model.presetProperty) {
			console.log('#' + simulations.length + ': ' + model.presetProperty.title + ' (' + model.presetProperty.year + ')');
			seriesTitle = model.presetProperty.title;
			secondHandFlag = model.presetProperty.secondHand;
		} else if (model.presetLeasing) {
			console.log('#' + simulations.length + ': ' + model.presetLeasing.title + ' (' + model.presetLeasing.year + ')');
			seriesTitle = model.presetLeasing.title;
		}

		if (seriesTitle) {
			// Take first two words:
			seriesTitle = seriesTitle.split(' ').slice(0, 2).join(' ');
		} else {
			seriesTitle = '#' + simulations.length;
		}

		seriesTitle += ' <i>['; // same as .flag CSS selector
		seriesTitle += model.owner == 'business' ? 'B' : 'P';
		if (model.ownership == 'leasing') {
			seriesTitle += 'L';
		}
		if (secondHandFlag) {
			seriesTitle += 'U';
		}
		if (model.fuelType == 'benzine') {
			seriesTitle += 'b';
		} else if (model.fuelType == 'phev') {
			seriesTitle += 'p';
		} else if (model.fuelType == 'diesel') {
			seriesTitle += 'd';
		} else if (model.fuelType == 'electro') {
			seriesTitle += 'e';
		}
		seriesTitle += ']</i>';

		// (B1) To convert from brutto to netto:
		var nettoConversionRatio = 1 - utilityService.safeParseFloat(model.taxBox) / 100;
		// (B2) To convert from netto to brutto (if necessary):
		var bruttoConversionRatio = 1;

		if (chartMode == null) {
			chartMode = model.owner;
		} else {
			if (chartMode != model.owner) {
				bruttoConversionRatio = 1 / nettoConversionRatio;
			}
		}

		if (chartMode == 'business') {
			simulationChart.yAxis.title.text = 'expences in brutto';
			simulationChart.yAxis.title.style = { color: 'navy' };
		} else {
			simulationChart.yAxis.title.text = 'expences in netto';
			simulationChart.yAxis.title.style = { color: 'darkgreen' };
		}

		//
		// Adds values for new series to the chart.
		// FIXME: Perhaps the concept with titles could be replaced by additional x-axes, for example as done by Grouped categories plugin:
		// http://www.highcharts.com/plugin-registry/single/11/Grouped-Categories
		//
		function addColumn(seriesLabel, chartData, isLineType) {
			var series = {
				name: seriesTitle + ' ' + seriesLabel.toLowerCase(),
				id:   seriesLabel + simulations.length,
				data: chartData
			};
			if (isLineType) {
				series.type = 'spline';
				series.marker = {
					lineWidth: 2
				};
				series.zIndex = 1; // draw lines above all columns
			} else {
				series.type = 'column';
				series.stack = 'series' + simulations.length;
			}
			simulationChart.series.push(series);
		}

		//
		// Returns the chart data row to fill data into for the given simulation year (2010, 2011, ...).
		//
		function addYearIfNecessary(simulationYear) {
			if (simulationChart.xAxis.categories.length <= simulationYear - currentYear) {
				simulationChart.xAxis.categories.push(simulationYear);
			}
		}

		//
		// Rounds given amount to two digits after comma.
		//
		function toMonthlyCosts(amount) {
			return '€' + Math.round(amount / 12 * 100) / 100;
		}

		// For business entity initial price should be a price without BPM and without VAT.
		var initialVehiclePrice = model.initialPrice;

		// Cumulative expenses for the whole period:
		var expence = 0;

		// Cumulative fuel price for the whole period:
		var fuelPrice = 0;
		var fuelPrices = [];

		if (model.addFuelCosts) {
			addColumn('Fuel', fuelPrices);
		}

		//
		// Calculates and adds annual fuel costs to the graph. Returns VAT over fuel costs.
		//
		function addFuelCostsIfNecessary(simulationYear) {
			var annualFuelVat = 0;
			if (model.addFuelCosts) {
				// (F1)
				var annualFuelPrice = model.kilometersPerYear / 100 * model.fuelVolumesPerHundred * utilityService.safeParseFloat(model.fuelPricePerVolume);
				annualFuelVat = annualFuelPrice * config.vatTax / 100;

				annualFuelPrice *= reverseVatTax;
				annualFuelPrice *= bruttoConversionRatio;

				fuelPrice += annualFuelPrice;
				expence += annualFuelPrice;
				fuelPrices.push(Math.round(fuelPrice));
			}
			return annualFuelVat;
		}

		// Cumulative operational costs for the whole period:
		var operationalCost = 0;
		var operationalCosts = [];
		var addOperationalCosts = model.addOperationalCosts && (model.ownership == 'property' || (model.ownership == 'leasing' && !model.operationalLeasing));

		if (addOperationalCosts) {
			addColumn('Operational costs', operationalCosts);
		}

		//
		// Calculates and adds annual operational vehicle costs to the graph. Returns VAT over these costs.
		//
		function addOperationalCostsIfNecessary(simulationYear) {
			var exraCostsVat = 0;
			if (addOperationalCosts) {
				// (O1) If both input fields (insurance costs + operational costs) are empty, the costs will be zero:
				var annualOperationalCost = utilityService.safeParseInt(model.insuranceCosts) * 12;

				if (model.operationalCosts) {
					annualOperationalCost += model.operationalCosts * reverseVatTax;
					exraCostsVat += model.operationalCosts * config.vatTax / 100;
				}

				annualOperationalCost *= bruttoConversionRatio;

				operationalCost += annualOperationalCost;
				expence += annualOperationalCost;
				operationalCosts.push(Math.round(operationalCost));
			}
			return exraCostsVat;
		}

		// Cumulative taxes for the whole period:
		var tax = 0;
		var taxes = [];
		// (V1) Multiplying by this variable effectively undoes VAT:
		var reverseVatTax = model.owner == 'business' ? 100 / (config.vatTax + 100) : 1;
		var addTaxes = model.addTaxes && (model.ownership == 'property' || model.owner == 'business');

		// Re-apply the view rule condition when adding taxes makes sense at all:
		if (addTaxes) {
			addColumn('Taxes', taxes);
		}

		//
		// Calculates annual vehicle tax (road tax). For business entity it is paid in brutto, for individual – in netto.
		//
		function getVehicleTax(simulationYear) {
			var vehicleTax = 0;
			// If CO₂ emission is zero, then vehicle tax is zero:
			if (model.coEmission > 0) {
				var taxes = config.vehicleTaxes[model.province].car;
				// PHEV are treated as normal vehicles:
				var fuelType = (model.fuelType == 'phev' ? 'benzine' : model.fuelType);
				for (var i = 0; i < taxes.length; i++) {
					if (taxes[i].weight == model.weight) {
						// Value in taxes is quarter brutto price:
						vehicleTax = (
							model.coEmission <= config.coEmissionReducedTax
								? taxes[i].taxesReduced[fuelType]
								: taxes[i].taxes[fuelType]
							) * 4;
						break;
					}
				}

				if (vehicleTax == 0) {
					model.message = 'Road tax for ' + model.weight + 'kg, ' + model.fuelType + ', ' + model.coEmission + ' CO₂ ' + ' in ' + model.province + ' was not found';
					return 0;
				}
			}

			// Netto vehicle tax is converted to brutto if necessary:
			vehicleTax *= bruttoConversionRatio;

			if (simulationYear == currentYear) {
				console.log('#' + simulations.length + ': Monthly road tax for ' + model.weight + 'kg, ' + model.fuelType + ', ' + model.coEmission + ' CO₂ in ' + model.province + ' is ' + toMonthlyCosts(vehicleTax));
			}

			return vehicleTax;
		}

		//
		// Calculate annual vehicle private use tax (bijtelling) for the given simulation year (2010, 2011, ...). Is applicable only for business entity and should be called when tax calculation is requested.
		//
		function getPrivateUseTax(simulationYear) {
			var privateUseTaxYear = simulationYear - (simulationYear - model.registrationYear) % config.privateUseTaxDurationYears;
			if (privateUseTaxYear > currentYear) {
				// Warning: taxation in the future is not known, but for sure it will not be better than in the last known year:
				privateUseTaxYear = config.privateUseTaxes[config.privateUseTaxes.length - 1].year;
			}
			var taxes;
			for (var i = 0; i < config.privateUseTaxes.length; i++) {
				if (config.privateUseTaxes[i].year == privateUseTaxYear) {
					taxes = config.privateUseTaxes[i].taxes[model.fuelType];
					if (taxes == null) {
						taxes = config.privateUseTaxes[i].taxes.other;
					}
					break;
				}
			}
			var cataloguePrice = model.cataloguePrice;
			var privateUseTax = 0;
			var i = 0;
			while (i < taxes.length - 1) {
				var cataloguePriceLimit = taxes[i].cataloguePriceLimit;
				if (model.coEmission == 0 && cataloguePriceLimit && cataloguePrice > cataloguePriceLimit) {
					cataloguePrice -= cataloguePriceLimit;
					privateUseTax += taxes[i].tax * cataloguePriceLimit;
				} else if (model.coEmission <= taxes[i + 1].coEmission) {
					break;
				}

				i++;
			}

			// Same formulae is used [here](https://www.dutchlease.nl/leasen/bijtelling-berekenen):
			privateUseTax = (privateUseTax + cataloguePrice * taxes[i].tax) * utilityService.safeParseFloat(model.taxBox) / 10000;

			console.log('#' + simulations.length + ': Monthly private use tax for ' + simulationYear + ', ' + model.coEmission + ' CO₂ is brutto ' + toMonthlyCosts(privateUseTax) + ' (max bijtelling ' + taxes[i].tax + '% over price €' + cataloguePrice + ' from year ' + privateUseTaxYear + ')');

			return privateUseTax;
		}

		//
		// Calculate VAT correction over the private part.
		//
		function getVatCorrection(simulationYear, vehicleExpencesVat) {
			vatPrivateFraction = 1 - model.businessKilometersPerYear / model.kilometersPerYear;

			// (V2) Calculate normal/proportional VAT:
			var normalVatCorrection = ((initialVehiclePrice - registrationTax) * config.vatTax / 100 / config.depreciationYears + vehicleExpencesVat) * vatPrivateFraction;
			// (V3) Calculate VAT correction for fixed rate:
			var fixedVatCorrection = model.cataloguePrice * ((simulationYear - currentYear) >= config.fixedVatCorrectionReducedYear ? config.fixedVatCorrectionReducedRate : config.fixedVatCorrectionRate) / 100;

			var vatCorrection = fixedVatCorrection < normalVatCorrection ? fixedVatCorrection : normalVatCorrection;

			console.log('#' + simulations.length + ': Monthly VAT correction ' + simulationYear + ' is brutto ' + toMonthlyCosts(vatCorrection) + ' (best of fixed ' + toMonthlyCosts(fixedVatCorrection) + ' and proportional ' + toMonthlyCosts(normalVatCorrection) + '), expences VAT is ' + toMonthlyCosts(vehicleExpencesVat));

			return vatCorrection;
		}

		//
		// Main calculation logic
		//
		if (model.ownership == 'property') {
			var registrationTax = 0;
			if (model.owner == 'business') {
				if (model.initialPriceWithTaxes) {
					// (P1) VAT over taxable part is returned to business entity.
					// Note that BPM can only be correctly calculated if catalogue price is known.
					registrationTax = getRegistrationTax(config, model);
					initialVehiclePrice = (initialVehiclePrice - registrationTax) * reverseVatTax + registrationTax;
					console.log('#' + simulations.length + ': Vehicle price minus VAT is €' + Math.round(initialVehiclePrice));
				}
			} else {
				// (P2)
				initialVehiclePrice *= bruttoConversionRatio;
				if (bruttoConversionRatio != 1) {
					console.log('#' + simulations.length + ': Vehicle price including ' + model.taxBox + '% tax is €' + Math.round(initialVehiclePrice));
				}
			}
			
			// Leftover vehicle price:
			vehiclePrice = initialVehiclePrice;

			if (!secondHandFlag) {
				vehiclePrice *= (1 - model.initialPriceDecrease / 100);
			}

			vehiclePrices = [];

			addColumn('Vehicle residual value', vehiclePrices, true);

			totalExpenses = [];

			// Cumulative total expense for the whole period (includes all other expenses minus leftover vehicle value):
			addColumn('Total expences', totalExpenses, true);

			for (var simulationYear = currentYear; simulationYear < currentYear + model.simulationYears; simulationYear++) {
				addYearIfNecessary(simulationYear);

				var vehicleExpencesVat =
					addFuelCostsIfNecessary(simulationYear) +
					addOperationalCostsIfNecessary(simulationYear);

				if (addTaxes) {
					var annualTax = getVehicleTax(simulationYear);

					if (model.owner == 'business') {
						if ((model.kilometersPerYear - utilityService.safeParseFloat(model.businessKilometersPerYear)) > config.privateUseTaxMaxKm) {
							annualTax += getPrivateUseTax(simulationYear);
							annualTax += getVatCorrection(simulationYear, vehicleExpencesVat);
						}
					} else if (model.businessKilometersPerYear) {
						var commuteTaxDeduction = model.businessKilometersPerYear * config.commuteTaxDeductionPerKm;
						if (simulationYear == currentYear) {
							console.log('#' + simulations.length + ': Monthly individual commute tax deduction is ' + toMonthlyCosts(commuteTaxDeduction));
						}
						annualTax -= commuteTaxDeduction;
					}

					tax += annualTax;
					expence += annualTax;
					taxes.push(Math.round(tax));
				}

				vehiclePrice *= (1 - model.annualPriceDecrease / 100);

				if (simulationYear == currentYear + model.simulationYears - 1) {
					console.log('#' + simulations.length + ': Leftover (initial minus current) vehicle value in ' + simulationYear + ' is €' + Math.round(initialVehiclePrice - vehiclePrice));
				}

				vehiclePrices.push(Math.round(vehiclePrice));
				totalExpenses.push(Math.round(initialVehiclePrice - vehiclePrice + expence));
			}
		} else {
			// Cumulative leasing fee for the whole period:
			var annualLeasingFee = model.leasingPrice * 12 * bruttoConversionRatio;
			var leasingFee = 0;
			var leasingFees = [];

			addColumn('Leasing fee', leasingFees);

			for (var simulationYear = currentYear; simulationYear < currentYear + model.simulationYears; simulationYear++) {
				addYearIfNecessary(simulationYear);

				addFuelCostsIfNecessary(simulationYear);
				addOperationalCostsIfNecessary(simulationYear);

				if (addTaxes) {
					var annualTax = getPrivateUseTax(simulationYear);
					tax += annualTax;
					taxes.push(Math.round(tax));
				}

				leasingFee += annualLeasingFee;
				leasingFees.push(Math.round(leasingFee));
			}
		}

		//console.log(angular.toJson(simulationChart));
	};

	var ratioChart = {
		chart: {
			type: 'area'
		},
		title: {
			text: 'Private/business ratio per expences costs'
		},
		plotOptions: {
			area: {
				pointInterval: 500
			}
		},
		xAxis: {
			title: {
				text: 'total annual expences'
			},
			labels: {
				formatter: function () {
					return utilityService.euroLabelFormatter(this.value);
				}
			}
		},
		yAxis: {
			title: {
				text: 'private/business ratio (0% = only business)',
			},
			labels: {
				format: '{value}%'
			},
			tickInterval: 10
		},
		tooltip: {
			headerFormat: '<span style="font-size: 80%">€{point.key}</span><br/>',
			pointFormat: '<span style="color:{point.color}">●</span> <b>{point.y}%</b>'
		},
		legend: {
			enabled: false
		},
		series: []
	};

	//
	// Generate and add new private/business chart data.
	// 3D chart: https://www.wolframalpha.com/input/?i=plot+(x*0.027*1.21)%2F(y*0.21)-(x)%2F(5*1.21),+y%3D0+to+1,x%3D20000+to+60000
	//
	var initRatioChart = function(config, model) {
		var data = [];
		ratioChart.series = [{
			data: data
		}];
		var registrationTax = getRegistrationTax(config, model);

		for (var expences = 0; expences <= 30000; expences += ratioChart.plotOptions.area.pointInterval) {
			var ratio = (model.cataloguePrice * config.fixedVatCorrectionRate / config.vatTax) / ((model.initialPrice - registrationTax) / config.depreciationYears / (1 + config.vatTax / 100) + expences);
			// The chart area where ratio get higher than 1 corresponds to the situation when proportional VAT correction is more profitable:
			if (ratio > 1) {
				//ratioChart.plotOptions.area.pointStart += ratioChart.plotOptions.area.pointInterval;
				ratio = 1;
			}
			// Rounding till first digit after comma:
			data.push(Math.round(ratio * 1000) / 10);
		}
	};

	//
	// Init section
	//
	Highcharts.setOptions({
		lang: {
			decimalPoint: ',',
			thousandsSep: '.'
		}
	});

	resetSimulationChart();

	// Exposed API:
	return {
		currentYear:          currentYear,
		sameMode:             function(mode) { return chartMode == null || chartMode == mode; },
		simulationChart:      simulationChart,
		resetSimulationChart: resetSimulationChart,
		switchToBusinessMode: switchToBusinessMode,
		addSimulation:        addSimulation,
		ratioChart:           ratioChart,
		initRatioChart:       initRatioChart
	};
}]);
