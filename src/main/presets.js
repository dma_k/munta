// Copyright 2017 Dmitry Katsubo, https://gitlab.com/dma_k/munta

//
// Presets for all cars.
// This service is stateless, so it is created using factory that returns singleton.
//
muntaModule.factory('Presets', function() {
	return {
		// Not available in the Netherlands:
		// * Audi Q5 Hybrid
		// * Peugeot 3008 Hybrid4 MPV -- cancelled after 2012. The first diesel hybrid on the market.
		// * Honda Insight Hybrid -- cancelled after 2014
		// * Ford Focus Electric -- https://en.wikipedia.org/wiki/Ford_Focus_Electric
		// * Nissan Pathfinder Hybrid -- cancelled after 2015
		// From 2018:
		// * Ford electric SUV -- by 2020
		// * Hyundai Kona SUV EV -- 40/64 kWh battery, 240..300 km range -- in 2019
		// * Kia Niro EV -- Will have the same engine and battery as Hyundai Ioniq EV, https://www.autokopen.nl/nieuwe-auto/kia/niro/45086-16gdi-hybrid-comfortline-dct6-aut#techniek
		// * Honda Insight -- from 2020, https://en.wikipedia.org/wiki/Honda_Insight#Third_generation_(2019%E2%80%93present)

		// weight in kg is 'ledig voertuig massa'
		// engineVolume in cubic centimetres (c³) or kilowatt-hours (kWh)
		// enginePower in horsepower (hp)
		// tankVolume in litres (for hybrid/other) or kWh (for electric)
		// baggageVolume (cargo space) in litres (baggage has extra wheel, if applicable)
		// cataloguePrice = fiscale waarde
		'common': [
			// https://www.autokopen.nl/nieuwe-auto/audi/a3-sportback/27313-14-tfsi-e-tron-s-tronic-sport
			{ title: 'Audi A3 Sportback sport 1.4 TFSI e-tron Automaat', year: 2017, cataloguePrice: 41110, weight: 1515, fuelType: 'benzine', enginePower: 204, tankVolume: 40, fuelVolumesPerHundred: 1.7, coEmission: 38, baggageVolume: 280 },
			// https://www.autokopen.nl/nieuwe-auto/bmw/2-serie-active-tourer/33336-225xe-iperformance
			{ title: 'BMW 2 Serie Active Tourer 225xe iPerformance Plug-in Hybrid', year: 2017, cataloguePrice: 41479, weight: 1635, fuelType: 'phev', engineVolume: 1499, enginePower: 224, tankVolume: 36, fuelVolumesPerHundred: 2, coEmission: 46, baggageVolume: 400 },
			// https://www.autokopen.nl/autonieuws/opel/ampera/ampera/
			// https://en.wikipedia.org/wiki/Chevrolet_Bolt#/media/File:BEV_EPA_range_comparison_2016-2017_MY_priced_under_50K_US.png
			{ title: 'Chevrolet Bolt EV', year: 2017, cataloguePrice: 35000 /* ? guess */, weight: 1625, fuelType: 'electro', enginePower: 200, tankVolume: 60, fuelVolumesPerHundred: 15, coEmission: 0, baggageVolume: 478 },
			// https://en.wikipedia.org/wiki/Chevrolet_Volt
			// http://www.thecarconnection.com/specifications/chevrolet_volt_2017_5dr-hb-lt
			{ title: 'Chevrolet Volt', year: 2017, cataloguePrice: 33000 /* ? guess */, weight: 1607, fuelType: 'benzine', engineVolume: 1498, enginePower: 150, tankVolume: 34, fuelVolumesPerHundred: 5.7, coEmission: 27, baggageVolume: 301 },
			// https://www.autokopen.nl/nieuwe-auto/ford/focus-hatchback/32986-10-ecoboost-trend-125-pk-at
			{ title: 'Ford Focus Trend 1.0 EcoBoost Automaat', year: 2017, cataloguePrice: 24445, weight: 1276, fuelType: 'benzine', engineVolume: 995, enginePower: 125, tankVolume: 55, fuelVolumesPerHundred: 5.5, coEmission: 125, baggageVolume: 277 },
			// https://www.autokopen.nl/nieuwe-auto/ford/focus-hatchback/42660--titanium-electric-powershift-aut
			{ title: 'Ford Focus Hatchback Electric', year: 2017, cataloguePrice: 39990, weight: 1644, fuelType: 'electro', enginePower: 143, tankVolume: 33.5, fuelVolumesPerHundred: 18, coEmission: 0, baggageVolume: 237 },
			// https://www.autokopen.nl/nieuwe-auto/ford/c-max/30497-15-ecoboost-150pk-titanium-auto
			// https://en.wikipedia.org/wiki/Ford_Focus_Electric
			{ title: 'Ford C-Max Titanium 1.5 EcoBoost Automaat Compact', year: 2017, cataloguePrice: 31960, weight: 1313, fuelType: 'benzine', engineVolume: 1499, enginePower: 150, tankVolume: 55, fuelVolumesPerHundred: 6.5, coEmission: 149, baggageVolume: 432 },
			// http://www.ford.com/cars/c-max/2017/models/c-max-hybrid-titanium/
			//{ title: 'Ford C-Max Plug-in Hybrid Titanium', year: 2017, cataloguePrice: 36995, weight: 1725, fuelType: 'phev', engineVolume: 1997, enginePower: 185, tankVolume: 53, fuelVolumesPerHundred: 2, coEmission: 46, baggageVolume: 200 },
			// https://www.autokopen.nl/nieuwe-auto/ford/c-max/27543-20-plug-in-hybrid-titanium-plus-cvt
			// Automatic transmission is only available on Titanium series.
			{ title: 'Ford C-Max Titanium 2.0 Plug-in Hybrid Automaat Compact', year: 2015, cataloguePrice: 34995, weight: 1725, fuelType: 'phev', engineVolume: 1997, enginePower: 137, tankVolume: 53, fuelVolumesPerHundred: 2, coEmission: 46, baggageVolume: 217 },
			// https://ovi.rdw.nl/ for plates 4-TSR-25
			{ title: 'Honda Insight 1.3 Trend Hybrid', year: 2014, cataloguePrice: 24600, weight: 1176, fuelType: 'benzine', engineVolume: 1339, enginePower: 88, tankVolume: 40, fuelVolumesPerHundred: 4.1, coEmission: 96, baggageVolume: 445 },
			// https://www.autokopen.nl/nieuwe-auto/hyundai/ioniq/37942-16-gdi-blue-hev-comfort (6-speed dual-clutch automatic gearbox):
			{ title: 'Hyundai Ioniq 1.6 GDI Blue HEV Comfort', year: 2017, cataloguePrice: 27074, weight: 1463, fuelType: 'benzine', engineVolume: 1580, enginePower: 141, tankVolume: 45, fuelVolumesPerHundred: 3.4, coEmission: 79, baggageVolume: 550 },
			// https://www.autokopen.nl/nieuwe-auto/hyundai/ioniq/41528--electric-comfort-aut
			// 165 km/h, 200 km EPA, charge power: 6.6 kW AC, charge time: 5:00
			{ title: 'Hyundai Ioniq EV Comfort', year: 2017, cataloguePrice: 31800, weight: 1455, fuelType: 'electro', enginePower: 120, tankVolume: 28, fuelVolumesPerHundred: 11.5, coEmission: 0, baggageVolume: 213 },
			// https://www.autokopen.nl/nieuwe-auto/hyundai/ioniq/51264--electric-comfort-88kw-aut
			// 165 km/h, 200 km EPA / 195 km EVDB / 280 km NEDC
			{ title: 'Hyundai Ioniq EV Comfort', year: 2018, cataloguePrice: 33067, weight: 1495, fuelType: 'electro', enginePower: 120, tankVolume: 28, fuelVolumesPerHundred: 11.5, coEmission: 0, baggageVolume: 350 },
			// ??
			// https://ev-database.nl/auto/1165/Hyundai-IONIQ-Electric
			// ? km/h, 265 EVDB, 294 km WLTP, charge power: 7.2 kW AC, charge time: 6:15
			//{ title: 'Hyundai Ioniq EV Comfort', year: 2019, cataloguePrice: ?, weight: 1420, fuelType: 'electro', enginePower: 137, tankVolume: 38.8, fuelVolumesPerHundred: ?, coEmission: 0, baggageVolume: 350 },
			// https://www.autokopen.nl/nieuwe-auto/hyundai/kona/59702--electric-comfort-64kw-aut
			// https://cleantechnica.com/2018/07/23/hyundai-kona-64-kwh-great-all-around-ev-get-one-quick/
			// https://www.evspecifications.com/en/model/2dee6c
			// 165 km/h, 415 km EPA / 449 WLTP, charging power: 7.2 kW AC, charging time: 10:30
			{ title: 'Hyundai Kona EV Comfort 64kWh', year: 2018, cataloguePrice: 38315, weight: 1660, fuelType: 'electro', enginePower: 204, tankVolume: 64, fuelVolumesPerHundred: 15.8, coEmission: 0, baggageVolume: 332 },
			// https://www.autokopen.nl/nieuwe-auto/hyundai/kona/63467-64kwh-electric-comfort-150kw-aut
			{ title: 'Hyundai Kona EV Comfort 64kWh', year: 2019, cataloguePrice: 40115, weight: 1660, fuelType: 'electro', enginePower: 204, tankVolume: 64, fuelVolumesPerHundred: 15.8, coEmission: 0, baggageVolume: 332 },
			// https://www.autokopen.nl/nieuwe-auto/hyundai/i30-wagon/32355-16-gdi-comfort-7dct
			{ title: 'Hyundai i30 Wagon i-Motion 1.6 GDI Comfort Automaat', year: 2017, cataloguePrice: 30494, weight: 1229, fuelType: 'benzine', engineVolume: 1591, enginePower: 135, tankVolume: 53, fuelVolumesPerHundred: 5.7, coEmission: 132, baggageVolume: 378 },
			// https://www.autokopen.nl/nieuwe-auto/kia/soul/42247--ev-executive-line-aut
			// 145 km/h, 150 km EPA / 455 km WLTP
			{ title: 'Kia Soul EV', year: 2017, cataloguePrice: 33495, weight: 1465, fuelType: 'electro', enginePower: 110, tankVolume: 27, fuelVolumesPerHundred: 18.6, coEmission: 0, baggageVolume: 281 },
			// https://www.autokopen.nl/nieuwe-auto/kia/niro/37191-16-gdi-hybrid-first-edition-dct6
			// 6-speed dual-clutch automatic gearbox
			{ title: 'Kia Niro 1.6 GDi Hybrid First Edition', year: 2016, cataloguePrice: 28495, weight: 1421, fuelType: 'benzine', engineVolume: 1580, enginePower: 141, tankVolume: 45, fuelVolumesPerHundred: 3.8, coEmission: 88, baggageVolume: 427 },
			// https://www.autokopen.nl/nieuwe-auto/kia/niro/64367-64kwh-dynamicline-150kw-aut
			// 167 km/h, 384 km EPA / 455 km WLTP, charge power: 7.2 kW AC, charge time: 10:30
			{ title: 'Kia eNiro 64kWh dynamicline', year: 2019, cataloguePrice: 41475, weight: 1712, fuelType: 'electro', enginePower: 204, tankVolume: 64, fuelVolumesPerHundred: 15.8, coEmission: 0, baggageVolume: 451 },
			// https://www.autokopen.nl/nieuwe-auto/lexus/ct/7491-200h-hybrid
			{ title: 'Lexus CT 200h Hybrid Automaat', year: 2017, cataloguePrice: 27755, weight: 1790, fuelType: 'benzine', enginePower: 136, tankVolume: 45, fuelVolumesPerHundred: 3.6, coEmission: 82, baggageVolume: 375 },
			// https://www.autokopen.nl/nieuwe-auto/nissan/leaf/42577--visia-30kw-accu-aut
			// 145 km/h, 172 km EPA
			{ title: 'Nissan Leaf Visia', year: 2017, cataloguePrice: 32740, weight: 1525, fuelType: 'electro', enginePower: 109, tankVolume: 30, fuelVolumesPerHundred: 16, coEmission: 0, baggageVolume: 370 },
			// https://www.autokopen.nl/nieuwe-auto/nissan/leaf/56927--2zero-edition-40kw-accu-aut
			// https://ev-database.nl/auto/1106/Nissan-Leaf
			// 150 km/h, 241 km EPA / 230 km EVDB / 270 km WLTP
			{ title: 'Nissan Leaf 2.0', year: 2018, cataloguePrice: 34140, weight: 1480, fuelType: 'electro', enginePower: 147, tankVolume: 40, fuelVolumesPerHundred: 18.6, coEmission: 0, baggageVolume: 435 },
			// https://www.autokopen.nl/nieuwe-auto/nissan/leaf/71380-62kwh-n-connecta-160kw-aut
			// https://ev-database.nl/auto/1144/Nissan-Leaf-eplus
			// 157 km/h, 350 km EVDB / 385 km WLTP, charge power: 6.6 kW AC, charge time: 10:45
			{ title: 'Nissan Leaf n-connecta 62kWh', year: 2019, cataloguePrice: 45000, weight: 1605, fuelType: 'electro', enginePower: 218, tankVolume: 62, fuelVolumesPerHundred: 17.1, coEmission: 0, baggageVolume: 400 },
			// https://www.autokopen.nl/nieuwe-auto/opel/ampera-e/41001--innovation-aut
			// De Ampera-E is gebaseerd op de Chevrolet Bolt, see https://www.autokopen.nl/autonieuws/2016/februari/40170-opel-ampera-e-bekende-naam-nieuw-concept
			// 161 km/h, 354 km EVDB / 375 km EPA / 380 km WLTP
			{ title: 'Opel Ampera-e', year: 2017, cataloguePrice: 40000, weight: 1611, fuelType: 'electro', enginePower: 204, tankVolume: 60, fuelVolumesPerHundred: 14.5, coEmission: 0, baggageVolume: 381 },
			// https://www.autokopen.nl/nieuwe-auto/opel/ampera-e/71107-60kwh-business-150kw-aut
			// https://ev-database.org/car/1051/Opel-Ampera-e
			// 150 km/h, 380 km WLTP, charge power: 7.4 kW AC, charge time: 9:15
			{ title: 'Opel Ampera-e business', year: 2019, cataloguePrice: 45549, weight: 1611, fuelType: 'electro', enginePower: 204, tankVolume: 60, fuelVolumesPerHundred: 14.5, coEmission: 0, baggageVolume: 381 },
			// https://www.autokopen.nl/nieuwe-auto/peugeot/3008/37568-active-12-puretech-130-eat6
			{ title: 'Peugeot 3008 Active 1.2 PureTech 130 EAT6 Automaat', year: 2017, cataloguePrice: 34830, weight: 1245, fuelType: 'benzine', engineVolume: 1199, enginePower: 131, tankVolume: 53, fuelVolumesPerHundred: 5.3, coEmission: 120, baggageVolume: 520 },
			// https://www.autokopen.nl/nieuwe-auto/tesla/model-3/72449-55kwh-standard-range-plus-rwd-175kw-aut
			// https://ev-database.org/car/1177/Tesla-Model-3-Standard-Range-Plus
			// https://www.evspecifications.com/en/model/bbc397
			// 225 km/h, 386 km EPA / 415 WLTP, charge power: 11 kW AC, charge time: 5:30
			{ title: 'Tesla Model 3 55kWh standard range plus RWD', year: 2019, cataloguePrice: 47800, weight: 1645, fuelType: 'electro', enginePower: 238, tankVolume: 55, fuelVolumesPerHundred: 14.2, coEmission: 0, baggageVolume: 542 },
			// https://www.autokopen.nl/nieuwe-auto/tesla/model-3/72448-75kwh-long-range-rwd-202kw-aut
			// https://ev-database.org/car/1100/Tesla-Model-3-Long-Range-RWD
			// https://www.evspecifications.com/en/model/3c3b98
			// 225 km/h, 499 km EPA / 600 WLTP, charge power: 11 kW AC, charge time: 8:00
			{ title: 'Tesla Model 3 75kWh long range RWD', year: 2019, cataloguePrice: 54100, weight: 1753, fuelType: 'electro', enginePower: 275, tankVolume: 75, fuelVolumesPerHundred: 15.0, coEmission: 0, baggageVolume: 542 },
			// https://www.autokopen.nl/nieuwe-auto/toyota/auris/39162-12t-aspiration
			{ title: 'Toyota Auris 1.2T Aspiration', year: 2017, cataloguePrice: 24730, weight: 1165, fuelType: 'benzine', engineVolume: 1197, enginePower: 116, tankVolume: 50, fuelVolumesPerHundred: 4.8, coEmission: 112, baggageVolume: 360 },
			// https://www.autokopen.nl/nieuwe-auto/toyota/auris/3904-14-d-4d-f-aspiration
			{ title: 'Toyota Auris 1.6 D-4D-F Aspiration', year: 2017, cataloguePrice: 29070, weight: 1270, fuelType: 'diesel', engineVolume: 1598, enginePower: 111, tankVolume: 50, fuelVolumesPerHundred: 4.1, coEmission: 104, baggageVolume: 360 },
			// https://www.autokopen.nl/nieuwe-auto/toyota/auris/21939-18-hybrid-aspiration-automaat
			{ title: 'Toyota Auris Aspiration 1.8 Hybrid Automaat', year: 2017, cataloguePrice: 25750, weight: 1285, fuelType: 'benzine', engineVolume: 1798, enginePower: 136, tankVolume: 45, fuelVolumesPerHundred: 3.6, coEmission: 82, baggageVolume: 360 },
			// https://www.dropbox.com/s/ivqr80t9fiemdrj/Toyota%20Auris%20pricelist%20%5B2015%5D.pdf
			{ title: 'Toyota Auris Dynamic 1.8 Hybrid Automaat', year: 2015, cataloguePrice: 26650, weight: 1285, fuelType: 'benzine', engineVolume: 1798, enginePower: 136, tankVolume: 45, fuelVolumesPerHundred: 3.6, coEmission: 82, baggageVolume: 360 },
			// https://www.dropbox.com/s/ivqr80t9fiemdrj/Toyota%20Auris%20pricelist%20%5B2015%5D.pdf
			{ title: 'Toyota Auris Aspiration Touring Sports 1.8 Hybrid Automaat', year: 2015, cataloguePrice: 27250, weight: 1310, fuelType: 'benzine', engineVolume: 1798, enginePower: 136, tankVolume: 45, fuelVolumesPerHundred: 3.5, coEmission: 82, baggageVolume: 600 },
			// https://www.autokopen.nl/nieuwe-auto/toyota/prius/34188-18-hybrid-aspiration-automaat
			{ title: 'Toyota Prius 1.8 Hybrid Aspiration Automaat', year: 2017, cataloguePrice: 29995, weight: 1350, fuelType: 'benzine', engineVolume: 1798, enginePower: 122, tankVolume: 43, fuelVolumesPerHundred: 3, coEmission: 70, baggageVolume: 343 },
			// https://www.autokopen.nl/nieuwe-auto/toyota/prius/37938-18-plug-in-hybrid-business-plus-autom
			{ title: 'Toyota Prius 1.8 Plug-in Hybrid Business Plus Automaat', year: 2017, cataloguePrice: 37995, weight: 1605, fuelType: 'phev', engineVolume: 1798, enginePower: 122, tankVolume: 43, fuelVolumesPerHundred: 1, coEmission: 22, baggageVolume: 443 },
			// https://www.autokopen.nl/nieuwe-auto/toyota/prius-plus/15241-18-hybrid-aspiration-automaat
			// Also is known as Toyota Prius+
			{ title: 'Toyota PriusV 1.8 Hybrid Aspiration Automaat', year: 2017, cataloguePrice: 33980, weight: 1475, fuelType: 'benzine', engineVolume: 1798, enginePower: 136, tankVolume: 45, fuelVolumesPerHundred: 4.4, coEmission: 101, baggageVolume: 232 },
			// https://www.autokopen.nl/nieuwe-auto/volkswagen/golf/41156--e-golf-aut
			// http://www.verbruiken.nl/elektrische-autos/volkswagen-e-golf
			// 150 km/h, 200 km EPA
			{ title: 'Volkswagen e-Golf', year: 2016, cataloguePrice: 37900, weight: 1510, fuelType: 'electro', enginePower: 136, tankVolume: 24, fuelVolumesPerHundred: 15.8, coEmission: 0, baggageVolume: 330 },
			{ title: 'Volkswagen Golf 7 1.2 TSI', year: 2015, cataloguePrice: 25952, weight: 1135, fuelType: 'benzine', enginePower: 110, tankVolume: 50, fuelVolumesPerHundred: 5, coEmission: 116, baggageVolume: 380 },
			// https://www.autokopen.nl/nieuwe-auto/volkswagen/passat-variant/39306-14-tsi-phev-gte-connected-series
			{ title: 'Volkswagen Passat Variant 1.4 Plug-in Hybrid GTE Connected Series', year: 2016, cataloguePrice: 44190, weight: 1635, fuelType: 'phev', engineVolume: 1395, enginePower: 218, tankVolume: 50, fuelVolumesPerHundred: 1.6, coEmission: 38, baggageVolume: 483 },
		],
		'property': [
			// https://www.audi.nl/nl/web/nl/audi-service/klantenservice/aanvragen.html#layer=/nl/web/nl/modellen/layer/brochure-form.html
			{ title: 'Audi A3 Sportback sport 1.4 TFSI e-tron Automaat', year: 2017, price: 42365, secondHand: false },
			// https://www.autokopen.nl/nieuwe-auto/bmw/2-serie-active-tourer/33336-225xe-iperformance
			{ title: 'BMW 2 Serie Active Tourer 225xe iPerformance Plug-in Hybrid', year: 2017, price: 42795, secondHand: false },
			// https://www.autoscout24.nl/aanbod/chevrolet-volt-neues-modell-leder-pdc-kamera-elektro-benzine-blauw-4a89a2c1-92ad-4eef-90a7-d7dc049aefe3?cldtidx=19
			{ title: 'Chevrolet Volt', year: 2017, price: 25800, secondHand: true },
			// ??? Is not on sale in Europe (https://www.chevrolet.nl/models/)
			{ title: 'Chevrolet Bolt EV', year: 2017, price: 33000, secondHand: false },
			// http://www.ford.nl/SBE/BrochuresEnPrijslijsten/Downloaden/DownloadenPersonenautos
			{ title: 'Ford Focus Trend 1.0 EcoBoost Automaat', year: 2017, price: 25155, secondHand: false },
			//
			{ title: 'Ford Focus Hatchback Electric', year: 2017, price: 32670, secondHand: false },
			// Automatic transmission is only available on Titanium series
			// http://www.automobile-catalog.com/car/2017/2140295/ford_c-max_1_5_ecoboost_182.html
			{ title: 'Ford C-Max Titanium 1.5 EcoBoost Automaat Compact', year: 2017, price: 32670, secondHand: false },
			// http://ww3.autoscout24.nl/classified/304460272
			// https://www.autotrack.nl/tweedehands/ford/c-max/32322761/extra-gegevens
			{ title: 'Ford C-Max Titanium 2.0 Plug-in Hybrid Automaat Compact', year: 2015, price: 24990, secondHand: true },
			// https://www.autoscout24.nl/aanbod/honda-insight-1-3-trend-hybrid-elektro-benzine-bruin-13597bd6-5c20-4dea-80d6-49eec8fe2980 (81457 km)
			{ title: 'Honda Insight 1.3 Trend Hybrid', year: 2014, price: 12945, secondHand: true },
			// http://ww3.autoscout24.nl/classified/297980181 – €28895
			// https://preuninger.hyundai.nl/configurator/hyundai-ioniq-hev/ – €30395 (incl €1000 rijklaarkosten)
			{ title: 'Hyundai Ioniq 1.6 GDI Blue HEV Comfort', year: 2017, price: 27950, secondHand: false },
			// http://ww3.autoscout24.nl/classified/303573818 – €27469
			// https://preuninger.hyundai.nl/configurator/hyundai-ioniq-hev/ – €32450 (incl €1000 rijklaarkosten)
			{ title: 'Hyundai Ioniq EV Comfort', year: 2017, price: 32450, secondHand: false },
			// https://h-static.nl/downloads/117/Prijslijst-Hyundai-IONIQ-Electric-EV.pdf
			{ title: 'Hyundai Ioniq EV Comfort', year: 2018, price: 33995, secondHand: false },
			// https://autopalace.nl/wp-content/uploads/2018/06/Prijzen-enspecificatiesKONAEV.pdf
			{ title: 'Hyundai Kona EV Comfort 64kWh', year: 2018, price: 39195, secondHand: false },
			// https://www.autoscout24.nl/aanbod/hyundai-kona-ev-comfort-64-kwh-elektrisch-grijs-18630f12-1a4c-44b0-b94e-701ede41bc2c?cldtidx=14
			{ title: 'Hyundai Kona EV Comfort 64kWh', year: 2019, price: 39995, secondHand: false },
			// https://preuninger.hyundai.nl/configurator/hyundai-i30-wagon/ – €26595 (incl €800 rijklaarkosten)
			{ title: 'Hyundai i30 Wagon i-Motion 1.6 GDI Comfort Automaat', year: 2017, price: 26595, secondHand: false },
			// https://www.autokopen.nl/voorraad-nieuw/kia/soul/7520265-kia-soul-ev-110pk-executiveline
			{ title: 'Kia Soul EV', year: 2017, price: 33750, secondHand: false },
			// http://auto.autoscout24.nl/Kia-Niro-Elektro/Benzine-Grijs-305826716
			{ title: 'Kia Niro 1.6 GDi Hybrid First Edition', year: 2016, price: 26495, secondHand: true },
			// https://www.autoscout24.nl/aanbod/kia-niro-e-niro-dynamicline-64kwh-nu-te-bestellen-455-km-a-elektrisch-grijs-caeb5e82-7026-401c-9e8a-ae4c6553c5a5?cldtidx=4 (01.2019)
			{ title: 'Kia eNiro 64kWh dynamicline', year: 2019, price: 43350, secondHand: false },
			// https://www.autokopen.nl/nieuwe-auto/lexus/ct/7491-200h-hybrid
			{ title: 'Lexus CT 200h Hybrid Automaat', year: 2017, price: 28995, secondHand: false },
			// https://www.autokopen.nl/nieuwe-auto/nissan/leaf/38421-visia-30kwh
			{ title: 'Nissan Leaf Visia', year: 2017, price: 33590, secondHand: false },
			// https://www.nissan.nl/voertuigen/nieuw/leaf/configurator.html
			{ title: 'Nissan Leaf 2.0', year: 2018, price: 33990, secondHand: false },
			// https://www.nissan.nl/voertuigen/nieuw/leaf/configurator.html
			{ title: 'Nissan Leaf n-connecta 62kWh', year: 2019, price: 45850, secondHand: false },
			// https://www.autokopen.nl/nieuwe-auto/opel/ampera-e/41001--innovation-aut
			{ title: 'Opel Ampera-e', year: 2017, price: 40995, secondHand: false },
			// https://www.autoscout24.nl/aanbod/opel-ampera-e-business-204-pk-wordt-verwacht-n19529-elektrisch-wit-eae82626-fa5b-4170-8c03-0f3edcf2df34?cldtidx=6 (01.2019)
			{ title: 'Opel Ampera-e business', year: 2019, price: 46699, secondHand: false  },
			// ???
			{ title: 'Peugeot 3008 Active 1.2 PureTech 130 EAT6 Automaat', year: 2017, price: 34830, secondHand: false },
			// https://www.tesla.com/nl_NL/model3/design
			{ title: 'Tesla Model 3 55kWh standard range plus RWD', year: 2019, price: 48818 },
			// https://www.tesla.com/nl_NL/model3/design
			{ title: 'Tesla Model 3 75kWh long range RWD', year: 2019, price: 59318 },
			// From https://www.toyota.nl/modellen/auris/index/prices
			{ title: 'Toyota Auris 1.2T Aspiration', year: 2017, price: 25595, secondHand: false },
			// From https://www.toyota.nl/modellen/auris/index/prices
			{ title: 'Toyota Auris 1.6 D-4D-F Aspiration', year: 2017, price: 29895, secondHand: false },
			// From https://www.toyota.nl/modellen/auris/index/prices
			// https://www.anwb.nl/auto/model/detail/t/2/uitvoering/492d5013-293d-482a-82a9-fcb95d567afb/nieuw/true
			{ title: 'Toyota Auris Aspiration 1.8 Hybrid Automaat', year: 2017, price: 27695, secondHand: false },
			// https://www.autoscout24.nl/aanbod/toyota-auris-1-8-hybrid-dynamic-navi-safetysense-elektro-benzine-grijs-8ebc90ae-6bfc-4f7a-861f-99f5dea896d2 (49695 km)
			{ title: 'Toyota Auris Dynamic 1.8 Hybrid Automaat', year: 2015, price: 19400, secondHand: true },
			// https://www.autoscout24.nl/aanbod/toyota-auris-1-8-hybrid-49-789-km-nw-model-touring-sports-elektro-benzine-wit-6bae8ee2-ff66-4170-9257-adaedb2f5c3a (49789 km)
			{ title: 'Toyota Auris Aspiration Touring Sports 1.8 Hybrid Automaat', year: 2015, price: 17949, secondHand: true },
			// https://www.autoscout24.nl/aanbod/toyota-auris-touring-sports-1-8-hybrid-lease-pro-limited-new-m-elektro-benzine-grijs-f8985e5c-63be-434d-9f47-aa7055f187f5 (15409 km)
			// actually is not Aspiration, but Lease Pro
			{ title: 'Toyota Auris Aspiration Touring Sports 1.8 Hybrid Automaat', year: 2015, price: 19849, cataloguePrice: 31672, secondHand: true },
			// https://www.autokopen.nl/nieuwe-auto/toyota/prius/34188-18-hybrid-aspiration-automaat
			{ title: 'Toyota Prius 1.8 Hybrid Aspiration Automaat', year: 2017, price: 30930, secondHand: false },
			// https://www.autokopen.nl/nieuwe-auto/toyota/prius/37938-18-plug-in-hybrid-business-plus-autom
			{ title: 'Toyota Prius 1.8 Plug-in Hybrid Business Plus Automaat', year: 2017, price: 37995, secondHand: false },
			// https://www.autokopen.nl/nieuwe-auto/toyota/prius-plus/15241-18-hybrid-aspiration-automaat
			{ title: 'Toyota PriusV 1.8 Hybrid Aspiration Automaat', year: 2017, price: 33980, secondHand: false },
			// https://www.autokopen.nl/voorraad-nieuw/volkswagen/golf/7605681-volkswagen-golf-e-golf-100-kw-136-pk
			{ title: 'Volkswagen e-Golf', year: 2016, price: 41740, secondHand: false },
			// https://www.autoscout24.nl/aanbod/volkswagen-golf-7-1-2-tsi-highline-apk-stoelverwarming-benzine-grijs-9357cf78-3184-8074-e053-e350040ac98a?cldtidx=4 (32662 km)
			{ title: 'Volkswagen Golf 7 1.2 TSI', year: 2015, price: 15999, secondHand: true },
			// http://ww3.autoscout24.nl/classified/301905978
			{ title: 'Volkswagen Passat Variant 1.4 Plug-in Hybrid GTE Connected Series', year: 2016, price: 45910, secondHand: true },
		],
		'leasing': [
			// From http://www.ford.nl/Zakelijk/Leasen/LeaseprijzenFordModellen
			{ title: 'Ford Focus Trend 1.0 EcoBoost Automaat', year: 2017, price: 349, privateLeasing: false },
			// https://www.fordprivatelease.com/ford-private-lease/model/ford-focus
			{ title: 'Ford Focus Trend 1.0 EcoBoost Automaat', year: 2017, price: 299, privateLeasing: true },
			// From https://www.toyota.nl/financieren-leasen-verzekeren/financieren/auris-private-lease.json#model/3
			{ title: 'Toyota Auris Aspiration 1.8 Hybrid Automaat', year: 2017, price: 449, privateLeasing: true },
			// From https://www.activlease.nl/outlet-operationele-lease/hyundai-kona-electric-5-deurs-comfort-64kwh-galactic-grey-levering-2019/
			{ title: 'Hyundai Kona EV Comfort 64kWh', year: 2019, price: 498, privateLeasing: true },
			// From https://www.activlease.nl/outlet-operationele-lease/tesla-model-3-4-deurs-75kwh-long-range-rwd/
			{ title: 'Tesla Model 3 75kWh long range RWD', year: 2019, price: 675, privateLeasing: true },
		]
	};
});
