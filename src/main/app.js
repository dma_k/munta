// Copyright 2017 Dmitry Katsubo, https://gitlab.com/dma_k/munta

//
// Problems found so far:
// * The button cannot be part of <label> as it does not receive onClick event: http://stackoverflow.com/a/30274613/267197
// * Various issues with <md-autocomplete>: http://stackoverflow.com/questions/42796420/issues-with-md-autocomplete-validation-broken-dropdown-position-numeric-value
// * [JavaScript Promises swallowing exceptions](http://jamesknelson.com/are-es6-promises-swallowing-your-errors/), [Resolve promises one after another (sequentially)](http://stackoverflow.com/questions/24586110)
// * Hotkeys are not provided by framework (use https://github.com/chieffancypants/angular-hotkeys/ or https://github.com/fupslot/angular-hotkeys-light)
//
"use strict";

var muntaModule = angular.module('MuntaBootstrap', ['ngMaterial', 'highcharts-ng', 'cfp.hotkeys']);

// Dependencies are explicitly listed to avoid problems with minification:
muntaModule.controller('MuntaInputController', ['$scope', '$q', '$http', 'UtilityService', 'ChartManager', 'Presets', 'hotkeys', function($scope, $q, $http, utilityService, chartManager, presets, hotkeys) {
	// Configure helpers which are used in HTML:
	$scope.angular = angular;               // AngularJS helpers (angular.isDefined(...), angular.isNumber(...)) that can be used in the view
	$scope.chartManager = chartManager;     // To start simulation / reset chart
	$scope.utilityService = utilityService; // To show dialog

	$scope.config = {
		applicationVersion: '0.4',
		tooltipDelay: 500,
		fuelTypes: [
			{ value: 'benzine', label: 'Benzine' },
			{ value: 'phev',    label: 'Benzine/Electricity' },
			{ value: 'diesel',  label: 'Diesel' },
			{ value: 'lpg3',    label: 'LPG/gas' },
			{ value: 'lpg',     label: 'LPG/other' },
			{ value: 'electro', label: 'Electricity' }
		],
		// FIXME: Load from the remote service, e.g. from http://www.brandstofprijzen.info/ or https://www.brandstof-zoeker.nl/Zuid-Holland/%27s-gravenhage/
		// Best fuel prices on 2020-06-04
		fuelPrices: {
			'benzine': 1.621,
			'phev':    1.621, // FIXME: What is the average price of dual charging?
			'diesel':  1.290,
			'lpg3':    0.747,
			'lpg':     0.747,
			'electro': 0.25
		},
		coEmissionReducedTax: 50, // vehicles with 1..50 CO₂ emission have reduced tax in years 2017..2020; the value should be in sync with Tax Office (https://www.belastingdienst.nl/wps/wcm/connect/nl/auto-en-vervoer/content/hulpmiddel-motorrijtuigenbelasting-berekenen)
		fixedVatCorrectionRate: 2.7, // 2.7% is calculated over catalogue price
		fixedVatCorrectionReducedRate: 1.5, // 1.5% is calculated over catalogue price if the vehicle is owned more than 4 years (see below)
		fixedVatCorrectionReducedYear: 4,
		depreciationYears: 5, // Number of years during which the vehicle will be depreciated (usually 5 as maximum depreciation rate is 20% per year)
		vatTax: 21, // 21% VAT tax
		commuteTaxDeductionPerKm: 0.19, // €0.19 per km are tax deductible for individual
		years: [], // are filled-in later
		provinces: [], // loaded from JSON
		weights: [], // loaded from JSON
		// Information was taken from https://en.wikipedia.org/wiki/Income_tax_in_the_Netherlands:
		taxBoxes: [
			{ value: 37.35, label: 'slice 1&2' },
			{ value: 49.50, label: 'slice 3' },
		],
		// Numbers are taken from
		// https://www.belastingdienst.nl/wps/wcm/connect/bldcontentnl/belastingdienst/prive/auto_en_vervoer/u_reist_naar_uw_werk/auto_van_uw_werkgever/afwijkende_regels/lagere_bijtelling_voor_milieuvriendelijke_autos/lagere_bijtelling_voor_milieuvriendelijke_autos
		// http://www.expatax.nl/kb/article/taxation-of-a-company-car-540.html
		privateUseTaxDurationYears: 5, // taxes in above table are valid for 5-years period
		privateUseTaxMaxKm: 500, // maximum number of km allowed for private use
		privateUseTaxes: [
			// The table starts from 2012, because if your vehicle was bought earlier, the 5-years duration has expired.
			// The information for year 2012 is incomplete because it is actually split into two halves. However in 2018 it anyway expires.
			// The left CO? emission boundary is exclusive, the right boundary is inclusive. For example for year 2015:
			// 0 CO?        = 4%
			// (0..50] CO?  = 7%
			// (50..82] CO? = 20%
			{ year: 2012, taxes: {
				diesel: [ { tax: 0 }, { coEmission: 0, tax: 0 }, { coEmission: 50, tax: 14 }, { coEmission: 91, tax: 20 }, { coEmission: 114, tax: 25 } ],
				other:  [ { tax: 0 }, { coEmission: 0, tax: 0 }, { coEmission: 50, tax: 14 }, { coEmission: 102, tax: 20 }, { coEmission: 132, tax: 25 } ],
			}},
			{ year: 2013, taxes: {
				diesel: [ { tax: 0 }, { coEmission: 0, tax: 0 }, { coEmission: 50, tax: 14 }, { coEmission: 88, tax: 20 }, { coEmission: 112, tax: 25 } ],
				other:  [ { tax: 0 }, { coEmission: 0, tax: 0 }, { coEmission: 50, tax: 14 }, { coEmission: 95, tax: 20 }, { coEmission: 124, tax: 25 } ],
			}},
			{ year: 2014, taxes: {
				diesel: [ { tax: 4 }, { coEmission: 0, tax: 7 }, { coEmission: 50, tax: 14 }, { coEmission: 85, tax: 20 }, { coEmission: 111, tax: 25 } ],
				other:  [ { tax: 4 }, { coEmission: 0, tax: 7 }, { coEmission: 50, tax: 14 }, { coEmission: 88, tax: 20 }, { coEmission: 117, tax: 25 } ],
			}},
			{ year: 2015, taxes: {
				other: [ { tax: 4 }, { coEmission: 0, tax: 7 }, { coEmission: 50, tax: 14 }, { coEmission: 82, tax: 20 }, { coEmission: 110, tax: 25 } ]
			}},
			{ year: 2016, taxes: {
				other: [ { tax: 4 }, { coEmission: 0, tax: 15 }, { coEmission: 50, tax: 21 }, { coEmission: 106, tax: 25 } ]
			}},
			{ year: 2017, taxes: {
				other: [ { tax: 4 }, { coEmission: 0, tax: 22 } ]
			}},
			{ year: 2018, taxes: {
				other: [ { tax: 4 }, { coEmission: 0, tax: 22 } ]
			}},
			{ year: 2019, taxes: {
				// For electrical vehicles the first 50000 are applied 4%, and everythingo over - 22%.
				// Vehicles that ride on hydrogen are applied 4% tax regardless the catalogue price.
				other: [ { tax: 4, cataloguePriceLimit: 50000 }, { coEmission: 0, tax: 22 } ]
			}},
			{ year: 2020, taxes: {
				other: [ { tax: 4, cataloguePriceLimit: 50000 }, { coEmission: 0, tax: 22 } ]
			}},
			{ year: 2021, taxes: {
				other: [ { tax: 22 } ]
			}}
		],
		// Import/registration (BPM) taxes are not necessary for calculation (as they should be included into the final price) and are provided for information only.
		// Correction for diesel is not performed.
		// Taken from
		// https://www.belastingdienst.nl/wps/wcm/connect/bldcontentnl/themaoverstijgend/brochures_en_publicaties/bpm-tarieven
		registrationTaxes: [
			// The left boundary is inclusive, the right boundary is exclusive. If CO₂ emission is zero, then BPM is zero.
			// Sample for 2017:
			// 0 CO₂         = €0
			// (0..76) CO₂   = €353 + €2 × CO₂ difference
			// [76..102) CO₂ = €505 + €66 × CO₂ difference
			{ year: 2015, taxes: [
				{ coEmission: 0,   basicTax: 175,   taxPerExtraGram: 6 },
				{ coEmission: 82,  basicTax: 667,   taxPerExtraGram: 69 },
				{ coEmission: 110, basicTax: 2599,  taxPerExtraGram: 112 },
				{ coEmission: 160, basicTax: 8199,  taxPerExtraGram: 217 },
				{ coEmission: 180, basicTax: 12539, taxPerExtraGram: 434 }
			]},
			{ year: 2016, taxes: [
				{ coEmission: 0,   basicTax: 175,   taxPerExtraGram: 6 },
				{ coEmission: 79,  basicTax: 649,   taxPerExtraGram: 69 },
				{ coEmission: 106, basicTax: 2512,  taxPerExtraGram: 124 },
				{ coEmission: 155, basicTax: 8588,  taxPerExtraGram: 239 },
				{ coEmission: 174, basicTax: 13129, taxPerExtraGram: 478 }
			]},
			{ year: 2017, fuelType: 'benzine', taxes: [
				{ coEmission: 0,   basicTax: 353,   taxPerExtraGram: 2 },
				{ coEmission: 76,  basicTax: 505,   taxPerExtraGram: 66 },
				{ coEmission: 102, basicTax: 2221,  taxPerExtraGram: 145 },
				{ coEmission: 150, basicTax: 9181,  taxPerExtraGram: 238 },
				{ coEmission: 168, basicTax: 13465, taxPerExtraGram: 475 }
			]},
			{ year: 2017, fuelType: 'phev', taxes: [
				{ coEmission: 0,   basicTax: 0,     taxPerExtraGram: 20 },
				{ coEmission: 30,  basicTax: 600,   taxPerExtraGram: 90 },
				{ coEmission: 50,  basicTax: 2400,  taxPerExtraGram: 300 }
			]},
			{ year: 2018, fuelType: 'benzine', taxes: [
				{ coEmission: 0,   basicTax: 356,   taxPerExtraGram: 2 },
				{ coEmission: 73,  basicTax: 502,   taxPerExtraGram: 63 },
				{ coEmission: 98,  basicTax: 2077,  taxPerExtraGram: 139 },
				{ coEmission: 144, basicTax: 8471,  taxPerExtraGram: 229 },
				{ coEmission: 162, basicTax: 12593, taxPerExtraGram: 458 }
			]},
			{ year: 2018, fuelType: 'phev', taxes: [
				{ coEmission: 0,   basicTax: 0,     taxPerExtraGram: 19 },
				{ coEmission: 30,  basicTax: 570,   taxPerExtraGram: 87 },
				{ coEmission: 50,  basicTax: 2310,  taxPerExtraGram: 289 }
			]},
			{ year: 2019, fuelType: 'benzine', taxes: [
				{ coEmission: 0,   basicTax: 360,   taxPerExtraGram: 2 },
				{ coEmission: 71,  basicTax: 502,   taxPerExtraGram: 60 },
				{ coEmission: 95,  basicTax: 1942,  taxPerExtraGram: 131 },
				{ coEmission: 139, basicTax: 7706,  taxPerExtraGram: 215 },
				{ coEmission: 156, basicTax: 11361, taxPerExtraGram: 429 }
			]},
			{ year: 2019, fuelType: 'phev', taxes: [
				{ coEmission: 0,   basicTax: 0,     taxPerExtraGram: 27 },
				{ coEmission: 30,  basicTax: 810,   taxPerExtraGram: 113 },
				{ coEmission: 50,  basicTax: 3070,  taxPerExtraGram: 271 }
			]},
			{ year: 2020, fuelType: 'benzine', taxes: [
				{ coEmission: 0,   basicTax: 366,   taxPerExtraGram: 2 },
				{ coEmission: 68,  basicTax: 502,   taxPerExtraGram: 59 },
				{ coEmission: 91,  basicTax: 1859,  taxPerExtraGram: 129 },
				{ coEmission: 133, basicTax: 7277,  taxPerExtraGram: 212 },
				{ coEmission: 150, basicTax: 10881, taxPerExtraGram: 424 }
			]},
			{ year: 2020, fuelType: 'phev', taxes: [
				{ coEmission: 0,   basicTax: 0,     taxPerExtraGram: 27 },
				{ coEmission: 30,  basicTax: 810,   taxPerExtraGram: 111 },
				{ coEmission: 50,  basicTax: 3030,  taxPerExtraGram: 267 }
			]},
		],
		insuranceTypes: [
			{ value: 35, label: 'obligatory' },
			{ value: 45, label: 'obligatory + limited casco' },
			{ value: 60, label: 'obligatory + full casco' }
		],
		initialPriceDecrease: 10,
		presets: presets
	};

	// Populate config years:
	for (var i = 0; i < 30; i++) {
		$scope.config.years.push({ value: chartManager.currentYear - i, label: chartManager.currentYear - i });
	}

	// Merge config presets:
	var commonPresets = {};

	$scope.config.presets['common'].forEach((item) => {
		commonPresets[item.title + '~' + item.year] = item;
	});

	for (var key in $scope.config.presets) {
		if (key == 'common') {
			continue;
		}

		$scope.config.presets[key] = $scope.config.presets[key].map((item) => {
			var commonItem = commonPresets[item.title + '~' + item.year];

			if (commonItem) {
				return angular.merge({}, item, commonItem);
			}

			console.log('The item ' + angular.toJson(item) + ' has no common description.');

			return item;
		});
	}

	// Default/initial values:
	$scope.model = {
		initialPrice:              25000,
		initialPriceWithTaxes:     true,
		leasingPrice:              300,
		operationalLeasing:        true,
		initialPriceDecrease:      $scope.config.initialPriceDecrease,
		annualPriceDecrease:       15,
		fuelType:                  $scope.config.fuelTypes[0].value,
		owner:                     'private',
		ownership:                 'property',
		addFuelCosts:              true,
		fuelVolumesPerHundred:     3.9,
		kilometersPerYear:         20000,
		businessKilometersPerYear: 2000,
		addTaxes:                  true,
		coEmission:                90,
		registrationYear:          chartManager.currentYear,
		addOperationalCosts:       true,
		insuranceCosts:            $scope.config.insuranceTypes[1].value.toString(),
		operationalCosts:          500,
		taxBox:                    $scope.config.taxBoxes[$scope.config.taxBoxes.length - 1].value.toString(),
		simulationYears:           5
	};

	//
	// Listeners
	//

	$scope.onPresetChange = function(ownership, preset) {
		$scope.model.ownership = ownership;

		if (ownership == 'property') {
			$scope.model.initialPrice = preset.price;
		} else {
			$scope.model.leasingPrice = preset.price;
			$scope.model.owner = preset.privateLeasing ? 'private' : 'business';
		}

		if (angular.isDefined(preset.secondHand)) {
			$scope.model.initialPriceDecrease = preset.secondHand ? 0 : $scope.config.initialPriceDecrease;
		}

		$scope.model.fuelType = preset.fuelType;
		$scope.onFuelTypeChange(true);
		$scope.model.fuelVolumesPerHundred = preset.fuelVolumesPerHundred;
		$scope.model.coEmission = preset.coEmission;
		$scope.model.registrationYear = preset.year ? preset.year : chartManager.currentYear;
		$scope.model.cataloguePrice = preset.cataloguePrice;

		var weights = $scope.config.weights;
		for (var i = weights.length - 1; i >= 0; i--) {
			if (preset.weight > weights[i].value) {
				$scope.model.weight = weights[i].value;
				break;
			}
		}
	};

	$scope.onFuelTypeChange = function(force) {
		if ($scope.model.fuelPricePerVolumeItem || force) {
			$scope.model.fuelPricePerVolumeItem = $scope.getFuelItemByType($scope.model.fuelType);
		}
	};

	//
	// Used by autocomplete input fields to filter items. Each item in given array should have "value" property
	// which if started with given value is returned.
	//
	$scope.filterItemsByValue = function(items, value) {
		if (value == '') {
			return items;
		}

		var result = items.filter(function(item) {
			return item.value.toString().startsWith(value);
		});

		if (result.length == 0) {
			// In case if no items matched, return all items:
			return items;
		}

		return result;
	};

	//
	// Returns the fuel type item by given fuel type.
	//
	$scope.getFuelItemByType = function(fuelType) {
		var fuelTypes = $scope.config.fuelTypes;
		for (var i = 0; i < fuelTypes.length; i++) {
			if (fuelTypes[i].value == fuelType) {
				return fuelTypes[i];
			}
		}
		return null;
	};

	//
	// Returns the fuel label by given fuel type.
	//
	$scope.getFuelLabelByType = function(fuelType) {
		var fuelItem = $scope.getFuelItemByType(fuelType);

		if (fuelItem) {
			return fuelItem.label;
		}
		return '?';
	};

	//
	// Returns the fuel unit label by given fuel type.
	//
	$scope.getFuelUnitByType = function(fuelType, longFormat) {
		if (longFormat) {
			return fuelType == 'electro' ? "kWh" : "litre";
		}

		return fuelType == 'electro' ? "kWh" : "L";
	};

	//
	// Checks that the element is visible and is enabled. Model name should be equal to element HTML ID.
	//
	$scope.isEnabled = function(elementId) {
		return $scope.model[elementId] && document.getElementById(elementId);
	}

	//
	// Check private/business mix condition and show a warning in that case.
	//
	$scope.addSimulation = function(event) {
		if (!chartManager.sameMode($scope.model.owner)) {
			if ($scope.model.owner == 'business') {
				utilityService.showDialog('warningAddBusiness', event).then(function(answer) {
					chartManager.switchToBusinessMode($scope.config);
					chartManager.addSimulation($scope.config, $scope.model);
				});
			} else {
				utilityService.showDialog('warningAddPrivate', event).then(function(answer) {
					chartManager.addSimulation($scope.config, $scope.model);
				});
			}
		} else {
			chartManager.addSimulation($scope.config, $scope.model);
		}
	};

	$scope.startNewSimulation = function(event) {
		chartManager.resetSimulationChart();
		$scope.addSimulation(event);
	}

	$scope.showRatioExpencesChart = function(event) {
		chartManager.calculateRatioExpencesChart($scope.config, $scope.model);
	}

	//
	// Hotkeys
	//
	hotkeys.add({
		combo:       'alt+a',
		description: 'Add simulation',
		callback:    $scope.addSimulation
	});
	hotkeys.add({
		combo:       'alt+s',
		description: 'Start new simulation',
		callback:    $scope.startNewSimulation
	});
	hotkeys.add({
		combo:       'alt+b',
		description: 'Show about',
		callback:    function() { utilityService.showDialog('aboutApplication'); }
	});

	//
	// Load external data from JSON and complete initialization.
	//
	$q.all([
		$http.get('data/nl_province.json').then(function(response) {
			var province = angular.fromJson(response.data);
			angular.forEach(province, function(value, key) {
				$scope.config.provinces.push({ value: key, label: value });
			});
		}),
		$http.get('data/taxes_netherlands.json').then(function(response) {
			$scope.config.vehicleTaxes = angular.fromJson(response.data);
			var taxes = $scope.config.vehicleTaxes.ZH.car; // can take any province - all should have the same list of weights
			for (var i = 0; i < taxes.length; i++) {
				$scope.config.weights.push({
					value: taxes[i].weight,
					label: i < taxes.length - 1 ? (taxes[i].weight + 1) + '..' + taxes[i+1].weight : '> ' + taxes[i].weight
				});
			}
			$scope.config.vehicleTaxesDate = new Date(response.headers('Last-Modified'));
		})
	]).then(function() {
		$scope.config.privateUseTaxesDate = new Date($scope.config.privateUseTaxes[$scope.config.privateUseTaxes.length - 1].year, 1, 1);
		$scope.model.province = $scope.config.provinces[10].value;
		$scope.model.weight = $scope.config.weights[8].value;
		$scope.model.message = null;
		$scope.onFuelTypeChange(true);
	}, function(error) {
		console.log(error);
		$scope.model.message = error;
	});
}]);

//
// Service with various helper functions.
//
muntaModule.service('UtilityService', ['$mdDialog', function($mdDialog) {
	this.euroLabelFormatter = function(value) {
		return '€' + Highcharts.numberFormat(value, 0);
	};

	this.safeParseInt = function(value) {
		var result;
		if (!value || value === '' || isNaN(result = parseInt(value, 10)) || result != value) {
			return 0;
		}
		return result;
	};

	this.safeParseFloat = function(value) {
		var result;
		if (!value || value === '' || isNaN(result = parseFloat(value, 10)) || result != value) {
			return 0;
		}
		return result;
	};

	this.showDialog = function(dialogId, event) {
		return $mdDialog.show({
			contentElement:      '#' + dialogId,
			parent:              angular.element(document.body),
			targetEvent:         event,
			clickOutsideToClose: true
		});
	};
}]);

// Directives to solve the problem with numeric model item check:
// http://stackoverflow.com/questions/19404969/angular-data-binding-input-type-number
// http://stackoverflow.com/questions/15072152/input-model-changes-from-integer-to-string-when-changed
muntaModule.directive('integer', ['UtilityService', function(utilityService) {
	return {
		require: 'ngModel',
		link: function(scope, element, attr, ctrl) {
			ctrl.$parsers.push(utilityService.safeParseInt);
		}
	};
}]);

muntaModule.directive('float', ['UtilityService', function(utilityService) {
	return {
		require: 'ngModel',
		link: function(scope, element, attr, ctrl) {
			ctrl.$parsers.push(utilityService.safeParseFloat);
		}
	};
}]);

muntaModule.filter('euroCurrency', ['UtilityService', function(utilityService) {
	return utilityService.euroLabelFormatter;
}]);

muntaModule.filter('float', function() {
	return function(value) {
		return Highcharts.numberFormat(value, 1);
	};
});

//
// Component that renders "show info dialog" icon-button.
//
muntaModule.component('showInfoDialog', {
	templateUrl: 'showInfoDialogTemplate',
	bindings: {
		dialogId: '@',
		init:     '&',
	},
	controller: ['UtilityService', function(utilityService) {
		// See template in which this function is called:
		this.showDialog = function(dialogId, init, event) {
			if (angular.isFunction(init)) {
				// Check
				//   https://stackoverflow.com/a/31440439/267197
				//   https://medium.com/front-end-hacking/angularjs-component-binding-types-my-experiences-10f354d4660#c888
				//   https://docs.angularjs.org/guide/component#component-based-application-architecture%23example-of-a-component-tree
				// for more information about callback variable binding:
				init();
			}
			return utilityService.showDialog(dialogId, event);
		}
	}]
});

//
// Component that renders info dialog.
// Nice guide for component transclusions: http://teropa.info/blog/2015/06/09/transclusion.html
// See why replace is deprecated: http://stackoverflow.com/questions/24194972/why-is-replace-deprecated-in-angularjs
//
muntaModule.component('infoDialog', {
	templateUrl: 'infoDialogTemplate',
	bindings: {
		title:    '@',
		dialogId: '@'
	},
	transclude: true,
	controller: ['$element', '$transclude', '$mdDialog', function($element, $transclude, $mdDialog) {
		$element.find('content').append($transclude());

		this.close = $mdDialog.cancel;
	}]
});

//
// Component that renders warning dialog.
//
muntaModule.component('warningDialog', {
	templateUrl: 'warningDialogTemplate',
	bindings: {
		dialogId: '@'
	},
	transclude: true,
	controller: ['$element', '$transclude', '$mdDialog', function($element, $transclude, $mdDialog) {
		$element.find('content').append($transclude());

		this.cancel = $mdDialog.cancel;
		this.confirm = $mdDialog.hide;
	}]
});
