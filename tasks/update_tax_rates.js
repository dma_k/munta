//
// This Grunt task updates the JSON data about taxes for cars and motorbikes in the Netherlands.
// Run this task from project root directory:
//
// $ grunt update
//
// The data is written in the following format:
//
// {
//    '<province code>' : {
//      'car'  : [ { weight : <from weight>, benzine : <price per quarter>, diesel : ..., lpg3 : ..., lpg : ... }, ... ]
//      'bike' : <motorbike tax per quarter>,
//    }
// }
//
// Where:
//   * weight = from this weight (exclusive)
//   * lpg3 = LPG3 and gas cars
//   * lpg = LPG/other cars
//
// See also:
// [Grunt: Creating tasks](http://gruntjs.com/creating-tasks)
// [Grunt.js: Custom Tasks](https://quickleft.com/blog/grunt-js-custom-tasks/)
module.exports = function (grunt) {
	grunt.registerTask('update', function() {
		var options = this.options({ dest : 'src/main/data/taxes_netherlands.json', province: '../src/main/data/nl_province.json' });
		var done = this.async();
		var tax = {};
		var province = require(options.province); // load provinces from external JSON

		var aSTT = []; // to make the script loaded from Tax Office happy; otherwise grunt results "Fatal error: aSTT is not defined"

		//
		// Evaluate the given JavaScript snippet and copy tax data for personal cars to global variable.
		//
		function addCarTaxesResponseHandler(responseBody, variableNamePrefix, carTaxCategoryKey, reject) {
			eval(responseBody);
			for (let key in province) {
				// The province may already exist:
				let outValues;
				let outValuesIndex = 0;
				let locateWeight = tax[key] != null;
				if (locateWeight) {
					outValues = tax[key].car;
				} else {
					outValues = [];
					tax[key] = { 'car' : outValues };
				}
				eval(variableNamePrefix + key).forEach((value) => {
					let inValues = value.split('#');
					let outValue = {};
					outValue.weight = parseInt(inValues[0]) - 1; // correction to make the weight rounded to 10 (1351->1350)
					outValue[carTaxCategoryKey] = { 'benzine' : parseInt(inValues[1]), 'diesel' : parseInt(inValues[2]), 'lpg3' : parseInt(inValues[3]), 'lpg' : parseInt(inValues[4]) };
					if (locateWeight) {
						while (outValuesIndex < outValues.length && outValues[outValuesIndex].weight != outValue.weight) {
							outValuesIndex++;
						}
						if (outValuesIndex == outValues.length) {
							reject(new Error(`Failed to locate weight ${outValue.weight} in the array with ${outValues.length} elements for province ${key}`));
						}
						outValues[outValuesIndex][carTaxCategoryKey] = outValue[carTaxCategoryKey];
					} else {
						outValues.push(outValue);
					}
				});
			}
		}

		//
		// Evaluate the given JavaScript snippet and copy tax data for personal bikes to global variable.
		//
		function addBikeTaxesResponseHandler(responseBody, reject) {
			eval(responseBody);
			eval('dataMotor').forEach((value) => {
				let inValues = value.split('#');
				let found = false;
				for (let key in province) {
					if (province[key] == inValues[0]) {
						tax[key].bike = parseInt(inValues[1]);
						found = true;
						break;
					}
				}
				if (!found) {
					reject(new Error(`The mapping for province ${inValues[0]} was not found`));
				}
			});
		}

		//
		// Makes HTTP GET request for given URL and calls the response handler to handle the response body.
		//
		function asyncParseTaxData(url, responseHandler) {
			return new Promise(function (resolve, reject) {
				grunt.log.ok(`Fetching URL '${url}'...`);
				var request = require('request'); // In contrast to Node.js 'http' module, this one handles redirects (HTTP 302) and HTTPs out-of-box.
				request(url, (error, response, body) => {
					const statusCode = response.statusCode;
					const contentType = response.headers['content-type'];

					if (error) {
						reject(error);
						return;
					}
					if (statusCode !== 200) {
						reject(new Error(`Request failed with status code ${statusCode}`));
						return;
					}
					if (!/^application\/x-javascript/.test(contentType)) {
						reject(new Error(`Invalid content-type: expected application/x-javascript but received ${contentType}`));
						return;
					}

					addCarTaxesResponseHandler(body, 'data', 'taxes', reject);
					addCarTaxesResponseHandler(body, 'dataPH_', 'taxesReduced', reject);
					addBikeTaxesResponseHandler(body, reject);

					resolve();
				});
			});
		}
		
		//
		// Common error handler.
		//
		function fail(error) {
			grunt.log.warn(error);
			// Notify Grunt about the failure:
			done(error);
		}

		asyncParseTaxData('https://www.belastingdienst.nl/common/js/iah/motorrijtuigenbelasting.js')
			.catch(fail)
			.then(() => {
				// Prettify with 2 spaces indent and write synchronously to a file (also supports --no-write Grunt option):
				grunt.file.write(options.dest, JSON.stringify(tax, null, 2));
				grunt.log.ok(`Tax info for ${Object.keys(tax).length} provinces is written to ${options.dest}.`);
				// Notify Grunt about the success:
				done();
			});

	});
};
