module.exports = function (grunt) {

	grunt.loadNpmTasks('grunt-contrib-clean');
	// Load additional tasks from "./tasks/" directory:
	grunt.loadTasks('tasks');

	grunt.initConfig({
		clean: ['build'],
		update: {
			options: {
				//dest: 'src/main/data/taxes_netherlands.json'
			}
		}
	});
	
	grunt.registerTask('default', []);
};
