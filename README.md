# Description

**Munta** project targets someone who would like to buy or to lease a car and would like to estimate related expenses, or compare different possibilities. The current and the only focus is laws and regulations in The Netherlands.

For example, the project tries to answer what is more profitable in the following situations:

* Hybrid or benzine car? Note that hybrid cars are more expensive and more heavier (hence results higher road taxes to be paid), but are more economic. In what term these expenses will pay back?
* Buy a car or prefer a private leasing? From one side full operational private leasing assumes that all maintenance costs are covered – these need to be estimated somehow. One the other side the bought vehicle has a leftover value, which when sold at the end of given period, partially compensates the expenses.
* How many kilometers one should ride so that diesel motor becomes more profitable?
* Buy a car as an individual or as a business entity?

For another experiment let's assume that the person has earned €100000 brutto per year.

* A private person has to pay income tax from it. One would assume that should be a highest tax box (assume 40%), as car is something the person can afford after he has bought food, clothes and paid house rent :sunglasses: Suppose the person gets €60000 netto after paying taxes. Then he buys a car for €30000 and starts spending the left €30000 for fuel, insurance, maintenance, etc. On the other hand the individual has tax return on business kilometers ridden...
* For business entity all car-related expenses are deductible. Hence he buys car from brutto and also gets VAT back (let's assume the car is electro hence has no registration (BPM) tax, then VAT from €30000 will be €5206), so the leftover will be €75206 and he continues to spend these money for fuel, insurance, maintenance, etc. Note that for example business entity has to pay €30000 × 2.7% = €810 VAT correction and €30000 × 4% × 40% = €480 private use tax every year which private person does not pay.

Now who will get his wallet empty sooner?


## What is not covered

* The project does not aim to cover tax regulations in countries other than The Netherlands. Most likely it will require re-work of the whole application (UI and controller).
* It is hardly possible to compare non-plug-in and [plug-in hybrid (PHEV)](https://en.wikipedia.org/wiki/Plug-in_hybrid). Last one suggests that you have your battery fully charged when fuel consumption is measured. Also some extra UI input is needed to learn what is the proportion of fuel tanking vs electricity charging.
* Does not calculate registration (BPM) tax addendum for diesel cars.


## Disclaimer

I am not a tax advisor or accountant. Below formulae may contain errors or may be totally wrong. The application makes a lot of assumptions and may not be a reliable source to calculate taxes!


# Online playground

The latest version of the application is available [here](https://www.centurion.link/munta/). If you like this application, please give in a star in GitLab!


# Algorithm explained

To get started let's introduce few formulae which we will use later. Also note that you will find the back-reference to these formulae from the source code.

* To get the price without VAT:
```math
reverseVatTax = \frac{100}{vatTax_\% + 100} \xRightarrow{vatTax = \frac{vatTax_\%}{100}} \frac{1}{vatTax + 1} \qquad\quad \text{(V1)}
```
* Provided that paid taxes are given as percentage, the formulae to convert from brutto to netto is:
```math
nettoConversionRatio = 1 - \frac{taxBox}{100} \qquad\quad \text{(B1)}
```
* Not difficult to see that to convert from netto to butto on can use
```math
bruttoConversionRatio = \frac{1}{nettoConversionRatio} \qquad\quad \text{(B2)}
```

We will add the following vehicles to our comparison chart to demo the application possibilities (current year is 2017):

* Ford C-Max Titanium 2.0 Plug-in Hybrid (2015, benzine, 2 L/km, 46 g/km) for €24990 as business owner
* Toyota Auris Aspiration 1.8 Hybrid Automaat (2017, benzine, 3.6 L/km, 82 g/km) for €27695 as private owner
* Ford Focus Trend 1.0 EcoBoost Automaat (2017, benzine, 5.5 L/km, 125 g/km) for €349 per month as business leasing

![First comparison](images/chart1.png)

## Initial step

- In the 1st scenario (unless `Includes VAT` checkbox is unchecked) for business entity VAT (21%) is deducted from vehicle price. VAT is calculated on the basis of brutto car price which is initial / catalogue price without [Vehicle registration tax (BPM)](https://nl.wikipedia.org/wiki/Belasting_van_personenauto's_en_motorrijwielen)) which is calculated as following:
```math
registrationTax = (basicTax + (coEmission - coEmission_{base}) \times taxPerExtraGram) \times \frac{initialVehiclePrice}{cataloguePrice} = \\
(€175 + (46 - 0) \times €6) \times \frac{€24990}{€34995} = €322.06 \qquad\quad \text{(T1)}
```
Note that registration tax for used car is calculated proportional to catalogue price :thinking_face: so it gets negligible over years. This formulae can be used by autohouse to calculate VAT to pay back to the tax office when it cells the car to the individual. Altogether initial Ford C-Max price without VAT (which is returned) with registration is:
```math
initialVehiclePrice_{noVat} = (initialVehiclePrice - registrationTax) \times reverseVatTax + registrationTax \stackrel{\text{(V1)}}{=} \\
(€24990 - €322.06) \frac{1}{1.21} + €322.06 = €20708.79 \qquad\quad \text{(P1)}
```
- In the 2nd scenario individuals have to pay VAT hence they start from initial vehicle price and they have to pay also income tax on this amount to convert it to brutto, so that it can be comparable with business expenses:
```math
initialVehiclePrice = €27695 \times bruttoConversionRatio \stackrel{\text{(B2)}}{=} €46158.33 \qquad\quad \text{(P2)}
```

## Iteration step

In the 1st scenario as this is a used car, only the 15% annual price depreciation is applied, so the cost of this car by the end of the year (perhaps) will be:
```math
vehiclePrice_{yearEnd} = initialVehiclePrice_{noVat} \times 85% = €17602.47
```
The residual cost of the car is represented with downgoing line.

Annual road tax for given car category (weight 1725kg ⊂ [1650kg..1750kg), benzine, reduced due to 46 CO₂ ≤ 50 CO₂) in ZH province in 2017 is $`136 \times 4 = €544`$.

Supposing that you ride 18000 km out of 20000 total km for private (which is more than 500 km limit). Then you have to pay [Private use tax (bijtelling)](https://nl.wikipedia.org/wiki/Priv%C3%A9gebruik_auto#In_de_inkomstenbelasting) and apply VAT correction:

* [Private use tax](https://nl.wikipedia.org/wiki/Priv%C3%A9gebruik_auto#In_de_inkomstenbelasting) for given car (46 CO₂ ⊂ (0..50] applies bijtelling 7% from year 2015) is a part of catalogue price of car to be added to (income) box 3, hence:
```math
privateUseTax = cataloguePrice \times bijtelling \times taxBox = €34995 \times 7% \times 40% = €979.86
```
It is fixed for 5 years and every 5 years new rate applies (application takes this into account) which means that 7% is fixed till 2020 and then new private use tax (22%) applies for next 5 years. As taxes in the future are not known, the application applies latest known private use tax for the future years e.g. private use tax from 2018 (22%) is applied to 2020.
* VAT correction requires that part of the VAT returned for car and operational costs (fuel, maintenance, spare parts) are paid back to tax office. It can be calculated in two ways:
  * Based on private/business kilometers ratio (requires daily kilometer recording/administration). Suppose that Ford C-Max really consumes 2 L per 100 km (that needs correction with respect to reality) and price per litre is €1.657, then VAT for fuel is $`\frac{20000 km}{100 km} \times 2 L \times €1.657 \times \frac{21}{100} = €139.19`$ and operational costs are $`€500 \times \frac{21}{100} = €105`$ (insurance is not applied VAT). Also assuming that number of years during which the vehicle will be depreciated is 5 (as maximum depreciation rate is 20% per year)
```math
normalVatCorrection = ((initialVehiclePrice_{noVat} - registrationTax) \times \frac{vatTax}{100} \times \frac{vatTax}{depreciationYears}) + vehicleExpencesVat) \times \frac{privateKilometersPerYear}{totalKilometersPerYear} = ((€20708.79 - €322.06) \times \frac{21}{100} \times \frac{1}{5}) + (€139.19 + €105)) \times \frac{18000 km}{20000 km} = €990.39 \qquad\quad \text{(V2)}
```
  * Fixed amount of 2.7% (or 1.5% after 4 years of use) is over catalogue price of the car:
```math
fixedVatCorrection = cataloguePrice \times \frac{2.7}{100} = €34995 \times \frac{2.7}{100} = €944.87 \qquad\quad \text{(V3)}
```
:warning: The application will calculate both options and choose the best one, in above example it is better to pay according to fixed VAT (€944.87 < €998.89). Interestingly, proportional VAT correction is better when private use is about 10-20%. You can see which VAT correction is more profitable if you input all data and click on Info button in tax row.

So total amount of taxes to pay annually is $`€544 + €979.86 + €944.87 = €2468.73`$

Annual price for fuel will be:
```math
fuelPrice = \frac{20000 km}{100 km} \times 2 L \times €1.657 \times reverseVatTax \stackrel{\text{(V1)}}{=} €547.76 \qquad\quad \text{(F1)}
```
and for operational costs:
```math
operationalCosts = €45 \times 12 + €500 \times reverseVatTax \stackrel{\text{(V1)}}{=} €953.22 \qquad\quad \text{(O1)}
```

The total expenses should also be added the car price drop so if one decides to cell the car at the end of the year, so the total loss in the 1st scenario will be $`€2468.73 + €547.76 + €953.22 + €20708.79 - €17602.47 = €7076.03`$ which is represented as upgoing line – the lower it goes, the better it is.

In the 2nd scenario as this is a new car, both 10% initial price and 15% annual price depreciation are applied so the cost of this car by the end of the year (perhaps) will be $`€46158.33 \times 90% \times 85% = 35311.12`$.

Road tax will be (weight 1285kg ⊂ [1250kg..1350kg), benzine, 82 CO₂) $`176 \times 4 \times bruttoConversionRatio \stackrel{\text{(B2)}}{=} €1173`$ brutto per year.

Generally above costs are just multiplied by the number of years for privately used cars, but for business several parameters may change within the emulation period (private use tax and VAT correction) which will effect the overall result. That on can see in year 2020 when 7% private use tax from 2015 raises to 22% and owning a car for business becomes not profitable anymore. Usually at this moment business cells the car to individual.

Note that individuals are entitled for tax deduction €0.19 per km to work which is $`2000 km \times €0.19 = €380`$ in this scenario. This amount is returned by the tax office so it may happen for electric cars that taxes to be paid are negative (since they freed from road taxes).

Other expenses are calculated straightforward. Annual price for fuel will be:
```math
fuelPrice = \frac{20000 km}{100 km} \times 3.6 L \times €1.657 \times bruttoConversionRatio \stackrel{\text{(B2)}}{=} €1988.4 \qquad\quad \text{(F1)}
```
brutto per year and for operational costs the amount it:
```math
operationalCosts = (€45 \times 12 + €500) \times bruttoConversionRatio \stackrel{\text{(B2)}}{=} €1733.33 \qquad\quad \text{(O1)}
```

The 3rd scenario is most simple as one has to pay to fuel, private use tax (bijtelling) and leasing fee using above formulae.

Private use tax for this car (125 CO₂ applies bijtelling 22% in 2017) is:
```math
privateUseTax = €24445 \times 22% \times 40% = €2151.16
```

There are no upgoing/downgoing lines in this scenario as the person does not own the car.

## Final step

In 1st scenario business entity has to pay VAT to tax office when it cells the car.


# License and copying

The whole project is licensed under [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html). Among others you get the following rights:

* You are allowed to use the software in any way you like.
* You are allowed to change the sources according to your needs.
* You can redistribute (modified or unmodified) version of the software.

But you have to accept these terms:

* There are no guarantees of any kind – if the software breaks anything for you for whatever reason then it's your problem.
* You may not redistribute the software and say you wrote it all yourself. In other words you cannot remove copyright marks.
* The sources of modified version of the software should be open.
* When you deploy this (modified or unmodified) software on the network, the link to [original site](https://gitlab.com/dma_k/munta) should be visible to the user.
